package ru.smlab.day27;

public class CreditCard {

    public static final String ISSUING_BANK_NAME = "Южно-северный западо-восточный банк";

    private String cardHolderName;

    private int limit;
    private int balance;

    public CreditCard(String cardHolderName) {
        this.cardHolderName = cardHolderName;
        this.limit = 50000;
        this.balance = 0;
    }

    private CreditCard(String cardHolderName, int limit, int balance) {
        this.cardHolderName = cardHolderName;
        this.limit = limit;
        this.balance = balance;
    }

    public boolean withdrawCash(int amount) {
        int expectedBalance = balance - amount;
        if (expectedBalance < 0 && Math.abs(expectedBalance) > limit) {
            System.out.println("Превышен лимит операций по карте.");
            return false;
        }

        balance = expectedBalance;

        System.out.println("Выдано наличных:" + amount);
        System.out.println("Остаток на карте:" + balance);
        return true;
    }

    private boolean forceWithdrawCash(int amount) {
        balance = balance - amount;
        System.out.println("Выдано наличных:" + amount);
        System.out.println("Остаток на карте:" + balance);
        return true;
    }


    @Override
    public String toString() {
        return "Кредитка выпущена" + ISSUING_BANK_NAME
                + "принадлежит: '" + cardHolderName
                + "', лимит=" + limit
                + ", баланс=" + balance;
    }
}
