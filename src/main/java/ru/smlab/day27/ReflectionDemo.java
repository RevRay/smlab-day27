package ru.smlab.day27;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class ReflectionDemo {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        CreditCard honestCard = new CreditCard("Ivan Petrov");
        System.out.println(honestCard);

        CreditCard fakeCard;
        Constructor[] constructors = honestCard.getClass().getDeclaredConstructors();
        for(Constructor c : constructors){
            System.out.println(c);
        }

//        Constructor c1 = honestCard.getClass().getDeclaredConstructor(String.class, int.class, int.class);
        Constructor c1 = CreditCard.class.getDeclaredConstructor(String.class, int.class, int.class);
        c1.setAccessible(true);
        fakeCard = (CreditCard) c1.newInstance("Alex K", 9090000, 100000);

        System.out.println(fakeCard);



        Method m = fakeCard.getClass().getDeclaredMethod("forceWithdrawCash", int.class);
        m.setAccessible(true);
        m.invoke(fakeCard, 9999999);
        System.out.println(fakeCard);

//        Object o = new String("fsd");
//        if (o.getClass().equals(String.class)) {
//
//        } else if (o.getClass().equals(Double.class))
    }
}
