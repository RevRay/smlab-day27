package ru.smlab.day27;

import java.lang.reflect.Field;

public class BreakingTheStringDemo {

    public static final String CONSTANT = "I am immutable!";

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        System.out.println(CONSTANT);
        System.out.println(CONSTANT.hashCode());

        Field stringDataField = CONSTANT.getClass().getDeclaredField("value");
        //право творить всякую дичь
        stringDataField.setAccessible(true);
        stringDataField.set(CONSTANT, "Welcome to Dark Souls".getBytes());

        System.out.println(CONSTANT);
        System.out.println(CONSTANT.hashCode());
    }
}
