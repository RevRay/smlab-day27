package ru.smlab.day27;

import java.lang.reflect.Field;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) throws Exception {
        Box box = new Box();
        System.out.println(box.getClass().getPackageName());
        String s = "";

        Field[] fields = box.getClass().getFields();
        for(Field f : fields) {
            System.out.println(f.getName());
        }

        Field[] fields1 = box.getClass().getDeclaredFields();
        for(Field f : fields1) {
            System.out.println(f.getName());
        }

        Field destinationField = box.getClass().getDeclaredField("destination");
        destinationField.setAccessible(true);
        System.out.println(destinationField.get(box));
        destinationField.set(box, "Tver");

        System.out.println();
        System.out.println(destinationField.get(box));


    }
}
