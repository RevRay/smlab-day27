package ru.smlab.day27.testframework;


public class MyUnitTests {

    //method name convention
    //test* *test
    //setUp
    //tearDown
    //Junit 4

    @Test
    void myLogic(){
        System.out.println("test1");
    }


    void sendUserRequest(){
        System.out.println("test2");
    }

    @Test
    void checkDBIntegreity(){
        System.out.println("test2");
    }

    @Test
    void test3(){
        System.out.println("test3");
    }

    void showCurrentTime(){
        System.out.println(System.currentTimeMillis());
    }

}
