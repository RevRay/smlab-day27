package ru.smlab.day27.testframework;

import java.lang.annotation.*;

//на что применяется аннотация?
//когда аннотация сохраняется?
//наследуется?
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Test {
}
