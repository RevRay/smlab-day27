package ru.smlab.day27.testframework;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
@Test
@BeforeClass
@AfterClass
 */
public class TestLauncher {
    public static void main(String[] args) {

        launchTests(new SmokeTests());
    }

    public static void launchTests(Object testSuite){
        Method[] declaredMethods = testSuite.getClass().getDeclaredMethods();
        for (Method testMethod : declaredMethods) {
            try {
                testMethod.setAccessible(true);
                String methodName = testMethod.getName();
//                if (methodName.startsWith("test")) {
                Annotation a = testMethod.getAnnotation(Test.class);
                if (a != null) {
                    testMethod.invoke(testSuite);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
